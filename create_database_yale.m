function images = create_database_yale()
% Read all the files from dataset, normalize the images, create single file
% of all cells containing images. All columns in a row contain images of
% same type and all rows correspond to different images in orl_faces
% database
Files = dir('yalefaces/');
subjectCounts = zeros(15,1);
for k = 1:length(Files)
	filename = Files(k).name;
	if strfind(filename,'01') > 0
		subjectCounts(1) = subjectCounts(1) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{1,subjectCounts(1)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'02') > 0
		subjectCounts(2) = subjectCounts(2) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{2,subjectCounts(2)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'03') > 0
			subjectCounts(3) = subjectCounts(3) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{3,subjectCounts(3)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'04') > 0
			subjectCounts(4) = subjectCounts(4) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{4,subjectCounts(4)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'05') > 0
			subjectCounts(5) = subjectCounts(5) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{5,subjectCounts(5)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'06') > 0
			subjectCounts(6) = subjectCounts(6) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{6,subjectCounts(6)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'07') > 0
			subjectCounts(7) = subjectCounts(7) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{7,subjectCounts(7)}= mat2gray(im2double(imresize(imread(pathname), [64, 64])));
	else if strfind(filename,'08') > 0
			subjectCounts(8) = subjectCounts(8) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{8,subjectCounts(8)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'09') > 0
			subjectCounts(9) = subjectCounts(9) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{9,subjectCounts(9)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'10') > 0
			subjectCounts(10) = subjectCounts(10) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{10,subjectCounts(10)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'11') > 0
			subjectCounts(11) = subjectCounts(11) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{11,subjectCounts(11)}= mat2gray(im2double(imresize(imread(pathname), [64, 64])));
	else if strfind(filename,'12') > 0
			subjectCounts(12) = subjectCounts(12) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{12,subjectCounts(12)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'13') > 0
			subjectCounts(13) = subjectCounts(13) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{13,subjectCounts(13)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'14') > 0
			subjectCounts(14) = subjectCounts(14) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{14,subjectCounts(14)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
	else if strfind(filename,'15') > 0
		subjectCounts(15) = subjectCounts(15) + 1;
        pathname = sprintf('yalefaces/%s', filename);
        images{15,subjectCounts(15)}= mat2gray(im2double(imresize(imread(pathname), [64, 64]))); 
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
