function [models] = pca2d(images, num_train, num_pca)
%% 2D PCA
% Read in images
num_classes =  size(images, 1);
[h, w] = size(images{1,1});
Z = zeros(h, w, num_train, num_classes);
average = zeros(h,w);
for aa= 1:num_classes
    sum = zeros(h, w);
    for tt = 1:num_train
        sum = sum + images{aa,tt};
    end
    avg{aa} = sum/num_train;
    average = average+sum/num_train;
end
average = average/ (num_classes);
    
for aa =1:num_classes
    G = zeros(h,w);
    for tt = 1:num_train
        Z = images{aa, tt} - average;
        G = G +Z'*Z;
    end
    [V,D] = eigs(G, num_pca);
    model.features = V;
    feature_images = [];
    for tt = 1:num_train
        feature_images =  horzcat(feature_images, V'*images{aa,tt});
    end
    model.eig = D;
    model.feature_images = feature_images;
    model.mean_feature = V'*avg{aa};
    model.dim = num_pca;
    models{aa} = model;
end
end
