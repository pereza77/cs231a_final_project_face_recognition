function [models] = weighted_2dpca(images, num_train, num_pca,num_subimages)
%% 2D PCA
% Read in images
num_classes =  size(images, 1);
[h, w] = size(images{1,1});
num_images_per_dimension = sqrt(num_subimages);
subimage_h = h/num_images_per_dimension;
subimage_w = w/num_images_per_dimension;
num_train_subimages = num_train*num_subimages;

average = zeros(subimage_h,subimage_w);
for aa= 1:num_classes
    sum = zeros(subimage_h, subimage_w);
    for tt = 1:num_train

		% Divide images into subimages
		subimages = mat2cell(images{aa,tt},ones(1,num_images_per_dimension).*subimage_h,ones(1,num_images_per_dimension).*subimage_w);

		for subimage_row = 1:num_images_per_dimension
			for subimage_col = 1:num_images_per_dimension
		        sum = sum + subimages{subimage_row,subimage_col};
			end
		end
    end
    avg{aa} = sum/num_train_subimages;
    average = average+sum/num_train_subimages;
end
average = average/ (num_classes);
    
for aa =1:num_classes
    G = zeros(subimage_h,subimage_w);
    for tt = 1:num_train
		% Divide images into subimages
		subimages = mat2cell(images{aa,tt},ones(1,num_images_per_dimension).*subimage_h,ones(1,num_images_per_dimension).*subimage_w);

		for subimage_row = 1:num_images_per_dimension
			for subimage_col = 1:num_images_per_dimension	
		        Z = subimages{subimage_row, subimage_col} - average;
        		G = G +Z'*Z;
			end
		end
    end
    [V,D] = eigs(G, num_pca);
    model.features = V;
    feature_images = [];
    for tt = 1:num_train
       	subimages = mat2cell(images{aa,tt},ones(1,num_images_per_dimension).*subimage_h,ones(1,num_images_per_dimension).*subimage_w);
		for subimage_row = 1:num_images_per_dimension
			for subimage_col = 1:num_images_per_dimension		
				feature_images =  horzcat(feature_images, V'*subimages{subimage_row,subimage_col});
			end
		end
    end
    model.eig = D;
    model.feature_images = feature_images;
    model.mean_feature = V'*avg{aa};
    model.dim = num_pca;
    models{aa} = model;
end
end
