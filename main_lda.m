% This is the main function for all work
clear all; close all;
disp('Reading in YALE Database');
images = create_database();
for num_train_images = 2:1:6
	for num_pca = 8:4:20

	disp('Computing LDA Models');
	models = lda(images, num_train_images, num_pca);

	disp('Building Classifiers via SVM');
	training_features=[];
	training_ids=[];
	for ii =1:size(models,2)
	    class_features = models{1,ii}.feature_images;
   		class_ids =ii*ones(1, size(class_features,2));
   		training_features = horzcat(training_features, class_features);
    	training_ids = horzcat(training_ids, class_ids);
	end
	% Build classifiers
	svm=multisvm(training_features, training_ids, models);
% Classify

disp('Classifying Test Images');
for aa = 1: size(images,1)
    for tt = 1:size(images,2)
        for ii= 1: size(models,2)
            results{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*images{aa,tt})'));
        end
    end
end
estimation= zeros(size(results,1),size(results,2));
for row=1:size(results,1)
    for column=1:size(results,2)
        [max_val, ind] = max(results{row,column}(:));
        if size(results{row,column}(:)==max_val, 2) ~=1
            ind=0;
        end
        estimation(row, column)=ind;
    end
end

true_results=zeros(size(results,1),size(results,2));
for row=1:size(results,1)
    true_results(row,:) = row* ones(1, size(results,2));
end

test = estimation == true_results;
accuracy(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));
dlmwrite('accuracy_lda_yale2',accuracy);

% Classify noisy images 

for aa = 1: size(images,1)

    for tt = 1:size(images,2)

        for ii= 1: size(models,2)

            % gaussian

            image = imnoise(images{aa,tt}, 'localvar', 0.05*ones(size(images{aa,tt},1), size(images{aa,tt},2)));

            %image = wiener2(image,[2 2]);

            results1{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'localvar', 0.10*ones(size(images{aa,tt},1), size(images{aa,tt},2)));

            %image = wiener2(image,[2 2]);

            results2{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'localvar', 0.15*ones(size(images{aa,tt},1), size(images{aa,tt},2)));

            %image = wiener2(image,[2 2]);

            results3{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'localvar', 0.20*ones(size(images{aa,tt},1), size(images{aa,tt},2)));

            %image = wiener2(image,[2 2]);

            results4{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            % speckle

            image = imnoise(images{aa,tt}, 'speckle', 0.05);

            results5{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'speckle', 0.10);

            results6{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'speckle', 0.15);

            results7{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'speckle', 0.20);

            results8{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            % salt & pepper

            image = imnoise(images{aa,tt}, 'salt & pepper', 0.05);

            %image = medfilt2(image);

            results9{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'salt & pepper', 0.10);

            %image = medfilt2(image);

            results10{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'salt & pepper', 0.15);

            %image = medfilt2(image);

            results11{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

            image = imnoise(images{aa,tt}, 'salt & pepper', 0.20);

            %image = medfilt2(image);

            results12{aa,tt}(ii) = sum(svmclassify(svm{ii}, (models{1,ii}.features'*image)'));

        end

    end

end

% correct = correct+ sum(results{ii}(ii,num_train_images+1:end)>30);

% incorrect = incorrect+ sum(results{ii}(ii,num_train_images+1:end)<=30);

% accuracy = correct/ (correct+incorrect)

true_results=zeros(size(images,1),size(images,2));

for row=1:size(results,1)

    true_results(row,:) = row* ones(1, size(results,2));

end



estimation= zeros(size(images,1),size(images,2));

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));
dlmwrite('accuracy0',accuracy);

%

results=results1;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy1(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy1',accuracy);

%

results=results2;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy2(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy2',accuracy);

%

results=results3;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy3(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy3',accuracy);

%

results=results4;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy4(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy4',accuracy);

%

results=results5;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy5(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy5',accuracy);

%

results=results6;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy6(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy6',accuracy);

%

results=results7;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy7(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy7',accuracy);

%

results=results8;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy8(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy8',accuracy);

%

results=results9;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy9(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy9',accuracy);

%

results=results10;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy10(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy10',accuracy);

%

results=results11;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy11(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy11',accuracy);

%

results=results12;

for row=1:size(results,1)

    for column=1:size(results,2)

        [max_val, ind] = max(results{row,column}(:));

        if size(results{row,column}(:)==max_val, 2) ~=1

            ind=0;

        end

        estimation(row, column)=ind;

    end

end

test = estimation == true_results;

accuracy12(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

dlmwrite('accuracy12',accuracy);


end

dlmwrite('accuracy0',accuracy);

dlmwrite('accuracy1',accuracy);

dlmwrite('accuracy2',accuracy);

dlmwrite('accuracy3',accuracy);

dlmwrite('accuracy4',accuracy);

dlmwrite('accuracy5',accuracy);

dlmwrite('accuracy6',accuracy);

dlmwrite('accuracy7',accuracy);

dlmwrite('accuracy8',accuracy);

dlmwrite('accuracy9',accuracy);

dlmwrite('accuracy10',accuracy);

dlmwrite('accuracy11',accuracy);

dlmwrite('accuracy12',accuracy);


end


