function [models] = pca(images, num_train, num_pca)
%% 2D PCA
% Read in images
num_classes =  size(images, 1);
[h, w] = size(images{1,1});
h = h*w;
w = 1;
Z = zeros(h, w, num_train, num_classes);
average = zeros(h,w);
for aa= 1:num_classes
    sum = zeros(h, w);
    for tt = 1:num_train
		sum = sum + reshape(images{aa,tt},h,w);
    end
    avg{aa} = sum/num_train;
    average = average+sum/num_train;
end
average = average/ (num_classes);
    
for aa =1:num_classes
	G = zeros(w,w);
    for tt = 1:num_train
        Z = reshape(images{aa, tt},h,w) - avg{aa};
		G = G+ Z'*Z;
		
    end
    [V,D] = eigs(G, num_pca);
    model.features = (G*V)';
    feature_images = [];
    for tt = 1:num_train
		
	        feature_images =  horzcat(feature_images, (G*V)' * reshape(images{aa,tt},h,w));
    end
    model.eig = D;
    model.feature_images = feature_images;
    model.mean_feature = (G*V)'*(avg{aa});
    model.dim = num_pca;
    models{aa} = model;
end
end
