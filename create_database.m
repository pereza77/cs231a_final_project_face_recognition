function images = create_database()
% Read all the files from dataset, normalize the images, create single file
% of all cells containing images. All columns in a row contain images of
% same type and all rows correspond to different images in orl_faces
% database
for ii = 1:40
    for jj = 1:10
        pathname = sprintf('orl_faces/s%d/%d.pgm', ii, jj);
        images{ii,jj}= mat2gray(im2double(imresize(imread(pathname), [64, 64])));
        %sum = sum + images(:,:,i);
    end
end
end
