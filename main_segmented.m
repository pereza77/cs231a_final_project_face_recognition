% This is the main function for all work
clear all; close all;
%cd cs231a/face_recognition/
disp('Processing YALE Database');
images = create_database_yale();

% Subimage parameters
[h,w] = size(images{1,1});
num_subimages = 4;
num_images_per_dimension = sqrt(num_subimages);
subimage_h = h/num_images_per_dimension;
subimage_w = w/num_images_per_dimension;

for num_train_images = 2:1:3;
for num_pca = 8:4:20;
disp('Computing Weighted 2DPCA models');
models = weighted_pca(images, num_train_images, num_pca, num_subimages);

disp('Building Classifiers using SVM');
training_features=[];
training_ids=[];
for ii =1:size(models,2)
    class_features = models{1,ii}.feature_images;
    class_ids =ii*ones(1, size(class_features,2));
    training_features = horzcat(training_features, class_features);
    training_ids = horzcat(training_ids, class_ids);
end
% Build classifiers
svm=multisvm(training_features, training_ids, models);
correct =0;
incorrect =0;
% Classify

disp('Classifying Images');
for aa = 1: size(images,1)
    for tt = 1:size(images,2)
        for ii= 1: size(models,2)
			classes = [];
	        subimages = mat2cell(images{aa,tt},ones(1,num_images_per_dimension).*subimage_h,ones(1,num_images_per_dimension).*subimage_w);

    	    for subimage_row = 1:num_images_per_dimension
        	    for subimage_col = 1:num_images_per_dimension
					classes = horzcat(classes, sum(svmclassify(svm{ii}, (models{1,ii}.features'*subimages{subimage_row,subimage_col})')));
				end
			end
            results{aa,tt}(ii) = mode(classes');
        end
    end
end
estimation= zeros(size(results,1), size(results,2));
for row=1:size(results,1)
    for column=1:size(results,2)
        [max_val, ind] = max(results{row,column}(:));
        if size(results{row,column}(:)==max_val, 2) ~=1
            ind=0;
        end
        estimation(row, column)=ind;
    end
end

true_results=zeros(size(results,1),size(results,2));
for row=1:size(results,1)
    true_results(row,:) = row* ones(1, size(results,2));
end

test = estimation == true_results;
accuracy(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));
end
dlmwrite('accuracy_weighted_pca_yale',accuracy);
end
