function [classifiers] =  build_classifiers(models, images)
num_classifiers = size(images,1);
Xi = [];
for classifier = 1: num_classifiers
    Xi = horzcat(Xi, models{1,classifier}.features);
end
% build yi
yi_len = size(Xi,2);
Yi = ones(num_classifiers, yi_len);
feature_per_class = size(Xi,2)/ num_classifiers;
ii=1:feature_per_class;
for class=1: num_classifiers
    Yi(class, (class-1)*feature_per_class+ii)= -1*Yi(class, (class-1)*feature_per_class+ii);
end
Yi=-1*Yi;
% norm1 = sum(Xi.^2,2);
% norm2 = sum(Xi'.^2,2);
% dist = (repmat(norm1 ,1,size(Xi',1)) + repmat(norm2',size(Xi,1),1) - 2*Xi*Xi');        
% K = exp(-0.5/param.sig^2 * dist);
