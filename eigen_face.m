%% 2D PCA
% Training image
images = zeros(112,92,10);
sum = zeros(112, 92);
for i = 1:10
    fname = strcat('s1\', int2str(i), '.pgm');
    images(:,:,i)= im2double(imread(fname));
    sum = sum + images(:,:,i);
end
avg = 0.1*sum;
imshow(avg);
G = zeros(92,92);
for i = 1:10
    G = G +(images(:,:,i)-avg)'* (images(:,:,i)-avg);
end

G = 0.1*G;
[V, D] = eigs(G, 5);

% Test image 10 and 8
im10 = im2double(imread('s1\10.pgm'));
B1 = im10*V;
im8 = im2double(imread('s1\8.pgm'));
B2 = im8*V;
im8_diff = im2double(imread('s2\8.pgm'));
B3 = im8_diff*V;

im2 = im2double(imread('s1\8.pgm'));
B4 = im2*V;

d1 = 0;
for j = 1:5
    d1 = d1 + norm(B1(:,j)-B2(:,j), 2);
end

d2 =0;
for j = 1:5
    d2 = d2 + norm(B1(:,j)-B3(:,j), 2);
end

d3 = 0;
for j = 1:5
    d3 = d3 + norm(B1(:,j)-B4(:,j), 2);
end
