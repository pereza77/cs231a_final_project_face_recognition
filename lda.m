function [models] = lda(images, num_train, num_pca)
%% 2D PCA
% Read in images
num_classes =  size(images, 1);
[h, w] = size(images{1,1});
%Z = zeros(h, w, num_train, num_classes);

% Compute Total Mean and Class Means
average = zeros(h,w);
for aa= 1:num_classes
    sum = zeros(h, w);
    for tt = 1:num_train
        sum = sum + images{aa,tt};
    end
    avg{aa} = sum/num_train;
    average = average+sum/num_train;
end
average = average/ (num_classes);
    
% Compute between-class and within-class scatter matrices S_b and S_w
S_b = zeros(size(images{1,1}));
for aa =1:num_classes
	S_b = S_b + num_train*((avg{aa}-average)*(avg{aa}-average)');
end
S_b = S_b./(num_classes*num_train);

for aa = 1:num_classes
	S_w = zeros(size(images{1,1}));
    for tt = 1:num_train
		S_w = S_w + (images{aa,tt}-avg{aa})*(images{aa,tt}-avg{aa})';
    end
 	S_w = S_w./(num_classes*num_train);
	S = S_w'*S_b;
    [V,D] = eigs(S, num_pca);
    model.features = V;
    feature_images = [];
    for tt = 1:num_train
        feature_images =  horzcat(feature_images, V'*images{aa,tt});
    end
    model.eig = D;
    model.feature_images = feature_images;
    model.mean_feature = V'*avg{aa};
    model.dim = num_pca;
    models{aa} = model;
end
end
