% This is the main function for all work
clear all; close all;
%cd cs231a/face_recognition/
disp('Processing ORL Database');
images = create_database();
for num_train_images = 4:1:6
for num_pca = 2:2:4;

disp('Computing PCA Models');
models = pca(images, num_train_images, num_pca);

disp('Training on SVM');
training_features=[];
training_ids=[];
for ii =1:size(models,2)
    class_features = models{1,ii}.feature_images;
    class_ids =ii*ones(1, size(class_features,2));
    training_features = horzcat(training_features, class_features);
    training_ids = horzcat(training_ids, class_ids);
end
% Build classifiers
svm=multisvm(training_features, training_ids, models);
% Classify

disp('Classifying Images');
for aa = 1: size(images,1)
    for tt = 1:size(images,2)
        for ii= 1: size(models,2)

            results{aa,tt}(ii) = sum(svmclassify(svm{ii}, ((models{1,ii}.features)*reshape(images{aa,tt},size(images{aa,tt},1)*size(images{aa,tt},1),1))'));
        end
    end
end
estimation= zeros(size(results,1),size(results,2));
for row=1:size(results,1)
    for column=1:size(results,2)
        [max_val, ind] = max(results{row,column}(:));
        if size(results{row,column}(:)==max_val, 2) ~=1
            ind=0;
        end
        estimation(row, column)=ind;
    end
end

true_results=zeros(size(results,1),size(results,2));
for row=1:size(results,1)
    true_results(row,:) = row* ones(1, size(results,2));
end

test = estimation == true_results;
accuracy(num_train_images,num_pca)  = sum(test(:))/(size(images,1)*size(images,2));

end

end
